# coding=utf-8
from django.http import JsonResponse
import json
from django.shortcuts import render

from app_models.forms import ContactForm, ApplicationForm
from app_models.models import *


# Create your views here.

def index(request):
    banner = Banner.objects.filter().first()
    services = Services.objects.all().order_by('id')[:3]
    offer = Offers.objects.all().order_by('id')[:4]
    projects = Projects.objects.all().order_by('id')[:1]
    contact_form = ContactForm()

    params = {
        'services': services,
        'banner': banner,
        'offers': offer,
        'projects': projects,
        'contacts': contacts,
        'form': contact_form
    }
    return render(request, 'app/index.html', params)


def service_single(request, id):
    service = Services.objects.get(id=id)
    return render(request, 'app/misc/services_single.html', {'service': service})


def offers_single(request, id):
    offer = Offers.objects.get(id=id)
    return render(request, 'app/misc/offers_single.html', {'offer': offer})


def project_single(request, id):
    project = Projects.objects.get(id=id)
    return render(request, 'app/misc/project_single.html', {'project': project})


def articles(request):
    contact_form = ContactForm()
    catid = request.GET.get('catid', -1)
    category = Category.objects.first()
    try:
        catid = int(catid)
        if catid > 0:
            category = Category.objects.get(id=catid)

    except ValueError:
        pass

    categories = Category.objects.all().order_by('id')[:7]
    articles = Articles.objects.filter(category=category, public=False).order_by('id')[:6]
    articles_locked = Articles.objects.filter(category=category, public=True).order_by('id')[:6]
    form = ContactForm(data=request.POST)
    save_contact = False
    if 'save_contact' in request.session:
        save_contact = True
    params = {
        'articles': articles,
        'categories': categories,
        'category': category,
        'locked': articles_locked,
        'form': form,
        'save_contact': save_contact
    }
    print save_contact
    return render(request, 'app/articles.html', params)


def article_single(request, id):
    article = Articles.objects.get(id=id)
    params = {
        'article': article
    }
    return render(request, 'app/misc/article_single.html', params)


def events(request):
    event = Events.objects.filter(last=False).order_by('id')[:6]
    contact_form = ContactForm()
    last = Events.objects.filter(last=True).order_by('id')[:8]
    params = {
        'events': event,
        'form': contact_form,
        'last': last
    }
    print event
    return render(request, 'app/events.html', params)


def event_single(request, id):
    event = Events.objects.get(id=id)
    application_form = ApplicationForm

    return render(request, 'app/misc/event_single.html', {'event': event, 'application': application_form})


def partners(request):
    application_form = ApplicationForm()
    params = {
        'application': application_form
    }
    return render(request, 'app/partners.html', params)


def contacts(request):
    params = {
        'location': 'lololo'
    }
    return render(request, 'app/contacts.html', params)


def books(request):
    contact_form = ContactForm()
    book = Books.objects.all().order_by('id')
    params = {
        'books': book,
        'form': contact_form
    }
    print params
    return render(request, 'app/books.html', params)


def single_book(request, id):
    contact_form = ContactForm()
    book = Books.objects.get(id=id)
    single1 = Books.objects.all().order_by('id')[:4]
    params = {
        'book': book,
        'single': single1,
        'form': contact_form,

    }
    print params
    return render(request, 'app/misc/single_book.html', params)


##### ajax ######

def save_contact(request):
    form = ContactForm(data=request.POST)
    if form.is_valid():
        instance = form.instance
        instance.save()

        request.session["save_contact"] = True

        return JsonResponse(dict(success=True))
    return JsonResponse(dict(success=False, message='Заполните поле!'))


def save_application(request):
    form = ApplicationForm(data=request.POST)
    if form.is_valid():
        instance = form.instance
        instance.save()

        request.session["save_application"] = True

        return JsonResponse(dict(success=True))
    return JsonResponse(dict(success=False, message='Заполните поле!'))


#### cart #######
def add_to_cart(request):
    id = request.POST.get('id')
    count = request.POST.get('count')
    cart = list()
    img = request.POST.get('img')
    price = request.POST.get('price')
    name = request.POST.get('name')
    if 'cart' in request.session:
        cart = json.loads(request.session['cart'])

    found = False
    for i in range(0, len(cart)):
        item = cart[i]
        if item['id'] == id:
            item['count'] = count
            cart[i] = item
            found = True
            break

    if not found:
        cart.append(dict(id=id, count=count,price=price, img=img, name=name))

    request.session['cart'] = json.dumps(cart)
    return JsonResponse(dict(success=True))


def delete_from_cart(request):
    id = request.POST.get('id')

    new_cart = list()
    cart = json.loads(request.session['cart'])

    for item in cart:

        if item['id'] != id:
            new_cart.append(item)

    request.session['cart'] = json.dumps(new_cart)
    return JsonResponse(dict(success=True))

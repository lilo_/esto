"""Esto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

from app.views import *

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^$', index, name='index'),
    url(r'^articles/$', articles, name='articles'),
    url(r'^article/(?P<id>\d+)$', article_single, name='article_single'),
    url(r'^events/$', events, name='events'),
    url(r'^event/(?P<id>[0-9]+)$', event_single, name='event_single'),
    url(r'^partners/$', partners, name='partners'),
    url(r'^contacts/$', contacts, name='contacts'),
    url(r'^books/$', books, name='books'),
    url(r'^book/(?P<id>[0-9]+)$', single_book, name='single_book'),
    url(r'^service/(?P<id>\d+)$', service_single, name='service_single'),
    url(r'^offer/(?P<id>\d+)$', offers_single, name='offers_single'),
    url(r'^project/(?P<id>\d+)$', project_single, name='project_single'),
    url(r'^contact_save/$', save_contact, name='save_contact'),
    url(r'^application_save/$', save_application, name='save_application'),
    url(r'^add_to_cart/$', add_to_cart, name='add'),
    url(r'^delete_from_cart/$', delete_from_cart, name='delete'),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

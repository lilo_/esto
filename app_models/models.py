# coding=utf-8
from __future__ import unicode_literals

from django.db import models
from ckeditor.fields import *


# Create your models here.


class Banner(models.Model):
    class Meta:
        db_table = 'banner'
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннеры'

    title = models.CharField(max_length=255, verbose_name='Наименование')
    short_description = models.CharField(max_length=1000, verbose_name='Краткое описание')
    description = RichTextField(verbose_name='Описание')
    image = models.ImageField(upload_to='', verbose_name='Изображение')

    def __unicode__(self):
        return self.title


class Services(models.Model):
    class Meta:
        db_table = 'services'
        verbose_name = 'Услугу'
        verbose_name_plural = 'Услуги'

    title = models.CharField(max_length=255, verbose_name='Ниаименование')
    description = RichTextField(verbose_name='Описаине')
    image = models.ImageField(upload_to='', verbose_name='Изображение')

    def __unicode__(self):
        return self.title


class Offers(models.Model):
    class Meta:
        db_table = 'offers'
        verbose_name = 'предложение'
        verbose_name_plural = 'предложения'

    title = models.CharField(max_length=255, verbose_name='Наименование')
    short_description = models.CharField(max_length=1000, verbose_name='краткое описание')
    description = RichTextField(verbose_name='Описание')
    image = models.ImageField(upload_to='', verbose_name='Изображение')

    def __unicode__(self):
        return self.title


class Projects(models.Model):
    class Meta:
        db_table = 'projects'
        verbose_name = 'проект'
        verbose_name_plural = 'проекты'

    title = models.CharField(max_length=255, verbose_name='Наименование')
    short_description = models.CharField(max_length=255, verbose_name='краткое описание')
    description = RichTextField(verbose_name='Описание')
    image = models.ImageField(upload_to='', verbose_name='Изображение')

    def __unicode__(self):
        return self.title


class Events(models.Model):
    class Meta:
        db_table = 'events'
        verbose_name = 'мероприятие'
        verbose_name_plural = 'мероприятия'

    name = models.CharField(max_length=255, verbose_name='имя спикера', default='')
    date = models.CharField(max_length=255, verbose_name='дата', default='')
    place = models.CharField(max_length=255, verbose_name='место проведения', default='')
    title = models.CharField(max_length=255, verbose_name='наименование', default='')
    short_description = RichTextField(verbose_name='краткое описание', default='')
    description = RichTextField(verbose_name='описание', default='')
    steps = models.ManyToManyField('EventProgram', verbose_name='шаги', default='')
    image = models.ImageField(upload_to='', verbose_name='изображение')
    last = models.BooleanField(verbose_name='Завершилось', default=False)

    def __unicode__(self):
        return self.title


class EventProgram(models.Model):
    class Meta:
        db_table = 'event_program'
        verbose_name = 'программу семинара'
        verbose_name_plural = 'программы семинаров'

    step_title = models.CharField(max_length=255, verbose_name='название шага')
    description = RichTextField(verbose_name='описание шага')

    def __unicode__(self):
        return self.step_title


class Books(models.Model):
    class Meta:
        db_table = 'trades'
        verbose_name = 'товар'
        verbose_name_plural = 'товары'

    title = models.CharField(max_length=255, verbose_name='наименование')
    autor = models.CharField(max_length=255, verbose_name='автор', default='')
    price = models.IntegerField(verbose_name='цена', default=0)
    description = RichTextField(verbose_name='описание')
    short_description = RichTextField(max_length=255, verbose_name='краткое описание', default='')
    image = models.ImageField(upload_to='', verbose_name='изображение', default='')

    def __unicode__(self):
        return self.title


class Articles(models.Model):
    class Meta:
        db_table = 'articles'
        verbose_name = 'статью'
        verbose_name_plural = 'статьи'

    title = models.CharField(max_length=255, verbose_name='Наименование')
    category = models.ForeignKey('Category', verbose_name='категория')
    short_description = models.CharField(max_length=255, verbose_name='краткое описание')
    description = RichTextField(verbose_name='описание', default='')
    public = models.BooleanField(verbose_name='скрыть?', default=False)

    def __unicode__(self):
        return self.title


class Category(models.Model):
    class Meta:
        db_table = 'category'
        verbose_name = 'категорию'
        verbose_name_plural = 'категории'

    name = models.CharField(max_length=255, verbose_name='Название')

    def __unicode__(self):
        return self.name


class Contacts(models.Model):
    class Meta:
        db_table = 'contacts'
        verbose_name = 'контакт'
        verbose_name_plural = 'контакты'

    mail = models.CharField(max_length=255, verbose_name='E-mail')
    name = models.CharField(max_length=255, verbose_name='Имя', null=True)

    def __unicode__(self):
        return self.mail


class Applications(models.Model):
    class Meta:
        db_table = 'applications'
        verbose_name = 'заявку'
        verbose_name_plural = 'заявки на участие'

    name = models.CharField(max_length=255, verbose_name='Имя')
    number = models.IntegerField(verbose_name='Номер телефона')
    mail = models.CharField(max_length=255, verbose_name='E-mail')

    def __unicode__(self):
        return self.name

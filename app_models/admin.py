from django.contrib import admin
from models import *


# Register your models here.


class ContactsAdmin(admin.ModelAdmin):
    list_display = 'mail name'.split()
    list_filter = ['name', 'mail']
    # list_editable = ['mail']


class ApplicationsAdmin(admin.ModelAdmin):
    list_display = 'name number mail'.split()
    list_filter = 'name number mail'.split()


admin.site.register(Banner)
admin.site.register(Services)
admin.site.register(Offers)
admin.site.register(Projects)
admin.site.register(Events)
admin.site.register(EventProgram)
admin.site.register(Books)
admin.site.register(Articles)
admin.site.register(Category)
admin.site.register(Contacts, ContactsAdmin)
admin.site.register(Applications, ApplicationsAdmin)

# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-14 12:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0019_books_autor'),
    ]

    operations = [
        migrations.AlterField(
            model_name='books',
            name='price',
            field=models.IntegerField(default=0, verbose_name='\u0446\u0435\u043d\u0430'),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-13 12:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0008_auto_20170213_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='events',
            name='place',
            field=models.CharField(default='', max_length=255, verbose_name='\u043c\u0435\u0441\u0442\u043e \u043f\u0440\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u044f'),
        ),
    ]

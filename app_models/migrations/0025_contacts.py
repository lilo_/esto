# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-16 14:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0024_articles_public'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contacts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mail', models.CharField(max_length=255, verbose_name='E-mail')),
                ('name', models.CharField(max_length=255, verbose_name='\u0418\u043c\u044f')),
            ],
            options={
                'db_table': 'contacts',
                'verbose_name': '\u043a\u043e\u043d\u0442\u0430\u043a\u0442\u044b',
            },
        ),
    ]

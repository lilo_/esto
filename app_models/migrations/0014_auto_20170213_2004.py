# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-13 14:04
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0013_auto_20170213_2002'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventprogram',
            options={},
        ),
        migrations.AlterModelTable(
            name='eventprogram',
            table=None,
        ),
    ]

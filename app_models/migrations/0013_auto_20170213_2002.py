# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-13 14:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0012_auto_20170213_2001'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventprogram',
            options={'verbose_name': '\u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u0435', 'verbose_name_plural': '\u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f'},
        ),
        migrations.AlterModelTable(
            name='eventprogram',
            table='event_program',
        ),
    ]

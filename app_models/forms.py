# coding=utf-8
from __future__ import unicode_literals

from django import forms
from django.db import models
from django.forms import ModelForm

from app_models.models import Contacts, Applications


class ContactForm(ModelForm):
    class Meta:
        model = Contacts
        exclude = ()

    name = forms.CharField(required=False)


class ApplicationForm(ModelForm):
    class Meta:
        model = Applications
        exclude = ()


